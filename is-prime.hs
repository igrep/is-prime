import           Data.Foldable
import           Math.NumberTheory.Primes.Testing
import           Safe
import           System.Environment


main :: IO ()
main = do
  args <- getArgs
  for_ args $ \arg -> do
    let result =
          case readMay arg of
              Just i ->
                if isPrime i
                  then "is prime"
                  else "is NOT prime"
              _ -> "Not an integer"
    putStrLn $ arg ++ ": " ++ result
