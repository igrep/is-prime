# is-prime

Just judge if given numbers are prime numbers or not.  
Powered by [arithmoi package](https://hackage.haskell.org/package/arithmoi).

# Example

```
> ./is-prime 12590 aaa 2 12590 11953
12590: is NOT prime
aaa: Not an integer
2: is prime
12590: is NOT prime
11953: is prime
```

# How to build

1. Install [stack](https://www.haskellstack.org)
1. Run `stack --resolver lts-9.6 setup` to install GHC. (Other version of lts would probably work).
1. Run `stack --resolver lts-9.6 ghc --package safe --package arithmoi is-prime.hs`
